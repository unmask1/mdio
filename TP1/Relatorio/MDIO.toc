\select@language {portuguese}
\contentsline {section}{\numberline {1}Parte 1}{3}
\contentsline {subsection}{\numberline {1.1}Determina\IeC {\c c}\IeC {\~a}o da lista de atividades}{3}
\contentsline {subsection}{\numberline {1.2}Modelo do caminho mais longo}{3}
\contentsline {subsection}{\numberline {1.3}Input}{4}
\contentsline {subsection}{\numberline {1.4}Output}{5}
\contentsline {subsection}{\numberline {1.5}Indica\IeC {\c c}\IeC {\~a}o do caminho mais longo (cr\IeC {\'\i }tico)}{5}
\contentsline {section}{\numberline {2}Parte 2}{6}
\contentsline {subsection}{\numberline {2.1}Modelo Programa\IeC {\c c}\IeC {\~a}o Linear}{6}
\contentsline {subsection}{\numberline {2.2}Input}{6}
\contentsline {subsection}{\numberline {2.3}Output}{7}
\contentsline {subsection}{\numberline {2.4}Diagrama de Gantt}{8}
\contentsline {subsection}{\numberline {2.5}Instante de tempo de uma atividade do caminho critico}{9}
\contentsline {subsection}{\numberline {2.6}Instante de tempo de uma atividade fora do caminho critico}{9}
\contentsline {section}{\numberline {3}Parte 3}{10}
\contentsline {subsection}{\numberline {3.1}Modelo de Programa\IeC {\c c}\IeC {\~a}o linear}{10}
\contentsline {subsection}{\numberline {3.2}Input}{10}
\contentsline {subsection}{\numberline {3.3}Output}{12}
\contentsline {subsection}{\numberline {3.4}Diagrama de Gantt}{13}
\contentsline {section}{\numberline {4}Parte 4}{14}
\contentsline {subsection}{\numberline {4.1}Gr\IeC {\'a}fico custo/dura\IeC {\c c}\IeC {\~a}o}{14}
\contentsline {section}{\numberline {5}Parte 5}{15}
\contentsline {subsection}{\numberline {5.1}Altera\IeC {\c c}\IeC {\~o}es Efectuadas ao Modelo da Parte 3}{15}
\contentsline {subsection}{\numberline {5.2}Input}{15}
\contentsline {subsection}{\numberline {5.3}Output}{17}
\contentsline {subsection}{\numberline {5.4}Diagrama de Gantt}{18}
